package by.boiko.crm;


import java.awt.*;

public class SimplePrinting extends Frame {

    public SimplePrinting(){
        Toolkit toolkit=Toolkit.getDefaultToolkit();
        PrintJob printJob=toolkit.getPrintJob(this,"Printing test",null);
        if(printJob!=null){
            Graphics graphics=printJob.getGraphics();
            graphics.drawString("Test printing in Java",10,50);
            printJob.end();
        }
    }

    public static void main(String[] arg){
        new SimplePrinting();
    }
}
