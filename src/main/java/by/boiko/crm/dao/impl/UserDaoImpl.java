package by.boiko.crm.dao.impl;


import by.boiko.crm.dao.UserDao;
import by.boiko.crm.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List loadByName(String tagName) {
        return sessionFactory.getCurrentSession().createQuery("select u from ZipCode u where u.info LIKE :tagName").setParameter("tagName", "%" + tagName + "%").list();
    }

    @Override
    public void save(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    @Override
    public User findById(Integer id) {
        return (User) sessionFactory.getCurrentSession().createQuery("select u from User u where id = :id").setParameter("id", id).uniqueResult();
    }

    @Override
    public List loadAll() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    @Override
    public List<User> loadUsers(int pageNum, int maxResult) {
        if (pageNum == 1){
            return sessionFactory.getCurrentSession().createQuery("from User").setFirstResult(0).setMaxResults(maxResult).list();
        }
        else {
            return sessionFactory.getCurrentSession().createQuery("from User").setFirstResult(pageNum * maxResult - maxResult).setMaxResults(maxResult).list();
        }

    }

    @Override
    public List<User> loadUserByName(int page, int maxResult, String name) {
        if (page == 1){
            return sessionFactory.getCurrentSession().createQuery("select u from User u where u.name LIKE :tagName").setParameter("tagName", "%" + name + "%").setFirstResult(0).setMaxResults(maxResult).list();
        }
        else {
            return sessionFactory.getCurrentSession().createQuery("select u from User u where u.name LIKE :tagName").setParameter("tagName", "%" + name + "%").setFirstResult(page * maxResult - maxResult).setMaxResults(maxResult).list();
        }
    }

    @Override
    public long loadAllCount() {
        return (long) sessionFactory.getCurrentSession().createQuery("SELECT COUNT(e) FROM User e").getSingleResult();
    }
}
