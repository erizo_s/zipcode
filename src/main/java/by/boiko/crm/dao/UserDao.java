package by.boiko.crm.dao;


import by.boiko.crm.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserDao {


    List loadByName(String tagName);

    void save(User user);

    User findById(Integer userId);

    List loadAll();

    long loadAllCount();

    List<User> loadUsers(int page, int maxResult);

    List<User> loadUserByName(int page, int maxResult, String searchText);
}
