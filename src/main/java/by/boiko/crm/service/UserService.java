package by.boiko.crm.service;


import by.boiko.crm.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    List findByName(String tagName);

    void saveOrUpdate(User user);

    User findById(Integer userId);

    List getAll();

    long getAllCount();

    List<User> getUsers(int page, int maxResult);

    List<User> getUserByName(int page, int maxResult, String searchText);
}
