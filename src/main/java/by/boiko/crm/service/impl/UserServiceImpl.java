package by.boiko.crm.service.impl;

import by.boiko.crm.dao.UserDao;
import by.boiko.crm.model.User;
import by.boiko.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public List findByName(String tagName) {
        return userDao.loadByName(tagName);
    }

    @Override
    @Transactional
    public void saveOrUpdate(User user) {
        userDao.save(user);
    }

    @Override
    @Transactional
    public User findById(Integer userId) {
        return userDao.findById(userId);
    }

    @Override
    @Transactional
    public List getAll() {
        return userDao.loadAll();
    }

    @Override
    @Transactional
    public long getAllCount() {
        return userDao.loadAllCount();
    }

    @Override
    @Transactional
    public List<User> getUsers(int page, int maxResult) {
        return userDao.loadUsers(page, maxResult);
    }

    @Override
    @Transactional
    public List<User> getUserByName(int page, int maxResult, String searchText) {
        return userDao.loadUserByName(page, maxResult, searchText);
    }


}
