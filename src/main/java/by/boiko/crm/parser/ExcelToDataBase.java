package by.boiko.crm.parser;


import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ExcelToDataBase {


    public static void main() throws SQLException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/test?characterEncoding=utf8", "root", "4123");
            con.setAutoCommit(false);
            PreparedStatement pstm = null;
            FileInputStream file = new FileInputStream("D:\\ZipCode.xls");
            POIFSFileSystem fs = new POIFSFileSystem(file);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            Row row;
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                DataFormatter dataFormatter = new DataFormatter();
                row = sheet.getRow(i);
                String code = dataFormatter.formatCellValue(row.getCell(0));
                String street = dataFormatter.formatCellValue(row.getCell(1));
                String sum = dataFormatter.formatCellValue(row.getCell(2));
                String sql = "INSERT INTO zip_code"
                        + "(code, street, sum) VALUES"
                        + "(?,?,?)";
                java.sql.PreparedStatement preparedStmt = con.prepareStatement(sql);
                preparedStmt.setString(1, code);
                preparedStmt.setString(2, street);
                preparedStmt.setString(3, sum);
                preparedStmt.executeUpdate();
                System.out.println("Import rows " + i + " " + code + sum);
            }
            con.commit();
            con.close();
            file.close();
            System.out.println("Success import excel to mysql table");
        } catch (ClassNotFoundException | SQLException | IOException e) {
            System.out.println(e);
        }
    }
}
