package by.boiko.crm.controller;


import by.boiko.crm.model.User;
import by.boiko.crm.parser.ExcelToDataBase;
import by.boiko.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * The controller determines methods for access to User service.
 */

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users")
    public ModelAndView getAllUsers() {
        ModelAndView mv = new ModelAndView("list");
        mv.addObject("users", userService.getAll());
        mv.addObject("counts", userService.getAllCount());
        return mv;
    }

    @RequestMapping(value = "/pars")
    public String loadToDataBase() throws IOException, SQLException {
        ExcelToDataBase.main();
        return "redirect:index";
    }

    // show add user form
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public ModelAndView showAddUserForm(@PathVariable("userId") Integer userId) {
        ModelAndView mv = new ModelAndView("show");
        mv.addObject("users", userService.findById(userId));
        return mv;
    }

    @RequestMapping(value = "/save")
    public String saveOrUpdateStudent(@ModelAttribute("userForm") User user) {
        userService.saveOrUpdate(user);
        return "redirect:/users";
    }

    /**
     * Open the form of creation and updating a user.
     *
     * @return page with form
     */
    @RequestMapping(value = "user/form")
    public ModelAndView openUserEditForm() {
        return new ModelAndView("form", "userForm", new User());
    }

    @RequestMapping(value = "/user/page/{page}")
    @ResponseBody
    public List<User> getNum(@PathVariable(value = "page")  int page) {
        ModelAndView mv = new ModelAndView("list");
        mv.addObject("counts", userService.getAllCount());
        int maxResult = 10;
        mv.addObject("users", userService.getUsers(page, maxResult));
        return userService.getUsers(page, maxResult);

    }

    @RequestMapping(value = "/user/page/{page}/{searchText}")
    @ResponseBody
    public List<User> getNum(@PathVariable(value = "page") int page, @PathVariable("searchText") String searchText) {
        ModelAndView mv = new ModelAndView("list");
        mv.addObject("counts", userService.getAllCount());
        int maxResult = 10;
        mv.addObject("users", userService.getUsers(page, maxResult));
        return userService.getUserByName(page, maxResult, searchText);
    }

    @RequestMapping(value = "/getTags/{tagName}", method = RequestMethod.GET)
    @ResponseBody
    public List getTags(@PathVariable(value = "tagName") String name) {
        return simulateSearchResult(name);
    }


    private List simulateSearchResult(String tagName) {
       return userService.findByName(tagName);
    }

}
