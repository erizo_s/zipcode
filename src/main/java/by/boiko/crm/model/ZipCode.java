package by.boiko.crm.model;

import javax.persistence.*;

@Entity
@Table(name = "zip_code")
public class ZipCode {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private int id;

    @Column(name = "code")
    private String code;

    @Column(name = "street")
    private String street;

    @Column(name = "SUM")
    private String info;

    public ZipCode() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
