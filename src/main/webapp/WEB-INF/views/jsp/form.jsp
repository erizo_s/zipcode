<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ru">


<jsp:include page="../fragments/header.jsp" />

<body>
<div class="container"><br>
    <c:choose>
        <c:when  test="${mode eq 'edit'}">
            <div class="well lead" align="center">Изменить информацию о пользователе</div>
        </c:when>

        <c:otherwise>
            <div class="well lead" align="center"><h1>Добавить получателя</h1></div>
        </c:otherwise>
    </c:choose><br><br>


    <form:form modelAttribute="userForm" class="form-horizontal" action="/save">
        <form:input type="hidden" path="id" id="id"/>

        <spring:bind path="name">
            <div class="form-group">
                <label for="exampleTextarea">Получатель</label>
                <form:textarea class="form-control" id="exampleTextarea" rows="3" path="name"  required="required"/>
            </div>
        </spring:bind>

        <spring:bind path="city">
            <div class="form-group row">
                <label class="col-xs-2 col-form-label">Город</label>
                <div class="col-xs-3">
                    <form:input path="city" type="text" class="form-control " id="city"  placeholder="Город" required="required" />
                </div>
            </div>
        </spring:bind>


             <div class="form-group row">
                <label class="col-xs-2 col-form-label">Информация</label>
                <div class="col-xs-8">
            <input class="form-control" type="text" id="search"  placeholder="Информация">
        <script>$(document).ready(function () {
            $("#search").typeahead({

                source: function (request, process) {
                    $.ajax({
                        url: "/getTags/" + request,
                        dataType: "json",
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var states = [];
                            $.each(data, function (i, state) {
                                states.push(state.info);
                                console.log(states);
                            });
                            process(states);
                        }
                    });
                }


            });
        });
        </script>
                </div>
             </div>

        <spring:bind path="street">
            <div class="form-group row">
                <label class="col-xs-2 col-form-label">Улица</label>
                <div class="col-xs-3">
                    <input  type="text" class="form-control " id="street"  placeholder="Улица" required="required" />
                    <script>$(document).ready(function () {
                        $("#street").typeahead({

                            source: function (request, process) {
                                $.ajax({
                                    url: "/getTags/" + request,
                                    dataType: "json",
                                    type: "GET",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        var states = [];
                                        $.each(data, function (i, state) {
                                            states.push(state.street);
                                            console.log(states);
                                        });
                                        process(states);
                                    }
                                });
                            }


                        });
                    });
                    </script>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="zip_code">
            <div class="form-group row">
                <label class="col-xs-2 col-form-label">Индекс</label>
                <div class="col-xs-3">
                    <form:input path="zip_code" type="text" class="form-control " id="zip_code"  placeholder="Индекс" required="required" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="number_house">
            <div class="form-group row">
                <label class="col-xs-2 col-form-label">Номер дома</label>
                <div class="col-xs-3">
                    <form:input path="number_house" type="text" class="form-control " id="number_house"  placeholder="Номер дома" required="required" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="number_flat">
            <div class="form-group row">
                <label class="col-xs-2 col-form-label">Номер квартиры</label>
                <div class="col-xs-3">
                    <form:input path="number_flat" type="text" class="form-control " id="number_flat"  placeholder="Номер квартиры" required="required" />
                </div>
            </div>
        </spring:bind>



        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-info">Сохранить</button>
            </div>
        </div>
    </form:form>
</div>

<jsp:include page="../fragments/footer.jsp" />
</body>

</html>