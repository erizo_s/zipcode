<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>
<body>
<div class="container">

    <h1 align="center">Заявка пациента</h1><br/>

    <div class="row">
        <label class="col-sm-2">Имя</label>
        <div class="col-sm-10">${users.name}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Город</label>
        <div class="col-sm-10">${users.lastName}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Улица</label>
        <div class="col-sm-10">${users.email}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Индекс</label>
        <div class="col-sm-10">${users.mobilePhone}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Дом</label>
        <div class="col-sm-10">${users.country}</div>
    </div>
    <div class="row">
        <label class="col-sm-2">Квартира</label>
        <div class="col-sm-10">${users.nameDepartment}</div>
    </div>



</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>